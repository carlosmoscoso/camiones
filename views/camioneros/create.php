<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Camioneros */

$this->title = 'Create Camioneros';
$this->params['breadcrumbs'][] = ['label' => 'Camioneros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camioneros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
