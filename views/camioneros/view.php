<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Camioneros */

$this->title = $model->codCamioneros;
$this->params['breadcrumbs'][] = ['label' => 'Camioneros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="camioneros-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codCamioneros' => $model->codCamioneros], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codCamioneros' => $model->codCamioneros], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codCamioneros',
            'nombre',
            'poblacion',
            'telefono',
            'direccion',
            'salario',
        ],
    ]) ?>

</div>
