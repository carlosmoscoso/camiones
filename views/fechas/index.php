<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fechas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fechas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idFechas',
            'codCamioneros',
            'codCamiones',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Fechas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idFechas' => $model->idFechas]);
                 }
            ],
        ],
    ]); ?>


</div>
