<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Provincias */

$this->title = 'Update Provincias: ' . $model->codProvincias;
$this->params['breadcrumbs'][] = ['label' => 'Provincias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codProvincias, 'url' => ['view', 'codProvincias' => $model->codProvincias]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="provincias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
