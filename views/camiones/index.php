<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Camiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Camiones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codCamiones',
            'modelo',
            'tipo',
            'potencia',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Camiones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codCamiones' => $model->codCamiones]);
                 }
            ],
        ],
    ]); ?>


</div>
