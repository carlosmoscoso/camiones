<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paquetes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Paquetes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codPaquetes',
            'descripcion',
            'direccionDes',
            'codCamioneros',
            'codProvincias',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Paquetes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codPaquetes' => $model->codPaquetes]);
                 }
            ],
        ],
    ]); ?>


</div>
