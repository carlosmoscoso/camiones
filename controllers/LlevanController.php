<?php

namespace app\controllers;

use app\models\Llevan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LlevanController implements the CRUD actions for Llevan model.
 */
class LlevanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Llevan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Llevan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codLlevan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Llevan model.
     * @param int $codLlevan Cod Llevan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codLlevan)
    {
        return $this->render('view', [
            'model' => $this->findModel($codLlevan),
        ]);
    }

    /**
     * Creates a new Llevan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Llevan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codLlevan' => $model->codLlevan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Llevan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codLlevan Cod Llevan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codLlevan)
    {
        $model = $this->findModel($codLlevan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codLlevan' => $model->codLlevan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Llevan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codLlevan Cod Llevan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codLlevan)
    {
        $this->findModel($codLlevan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Llevan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codLlevan Cod Llevan
     * @return Llevan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codLlevan)
    {
        if (($model = Llevan::findOne(['codLlevan' => $codLlevan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
