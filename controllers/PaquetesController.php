<?php

namespace app\controllers;

use app\models\Paquetes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaquetesController implements the CRUD actions for Paquetes model.
 */
class PaquetesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Paquetes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Paquetes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codPaquetes' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Paquetes model.
     * @param int $codPaquetes Cod Paquetes
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codPaquetes)
    {
        return $this->render('view', [
            'model' => $this->findModel($codPaquetes),
        ]);
    }

    /**
     * Creates a new Paquetes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Paquetes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codPaquetes' => $model->codPaquetes]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Paquetes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codPaquetes Cod Paquetes
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codPaquetes)
    {
        $model = $this->findModel($codPaquetes);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codPaquetes' => $model->codPaquetes]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Paquetes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codPaquetes Cod Paquetes
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codPaquetes)
    {
        $this->findModel($codPaquetes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Paquetes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codPaquetes Cod Paquetes
     * @return Paquetes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codPaquetes)
    {
        if (($model = Paquetes::findOne(['codPaquetes' => $codPaquetes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
