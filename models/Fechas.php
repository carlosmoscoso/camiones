<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fechas".
 *
 * @property int $idFechas
 * @property int|null $codCamioneros
 * @property int|null $codCamiones
 *
 * @property Camioneros $codCamioneros0
 * @property Camiones $codCamiones0
 */
class Fechas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fechas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codCamioneros', 'codCamiones'], 'integer'],
            [['codCamioneros'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['codCamioneros' => 'codCamioneros']],
            [['codCamiones'], 'exist', 'skipOnError' => true, 'targetClass' => Camiones::className(), 'targetAttribute' => ['codCamiones' => 'codCamiones']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idFechas' => 'Id Fechas',
            'codCamioneros' => 'Cod Camioneros',
            'codCamiones' => 'Cod Camiones',
        ];
    }

    /**
     * Gets query for [[CodCamioneros0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCamioneros0()
    {
        return $this->hasOne(Camioneros::className(), ['codCamioneros' => 'codCamioneros']);
    }

    /**
     * Gets query for [[CodCamiones0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCamiones0()
    {
        return $this->hasOne(Camiones::className(), ['codCamiones' => 'codCamiones']);
    }
}
