<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $codPaquetes
 * @property string $descripcion
 * @property string $direccionDes
 * @property int|null $codCamioneros
 * @property int|null $codProvincias
 *
 * @property Camioneros $codCamioneros0
 * @property Provincias $codProvincias0
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'direccionDes'], 'required'],
            [['codCamioneros', 'codProvincias'], 'integer'],
            [['descripcion', 'direccionDes'], 'string', 'max' => 50],
            [['codCamioneros'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['codCamioneros' => 'codCamioneros']],
            [['codProvincias'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['codProvincias' => 'codProvincias']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codPaquetes' => 'Cod Paquetes',
            'descripcion' => 'Descripcion',
            'direccionDes' => 'Direccion Des',
            'codCamioneros' => 'Cod Camioneros',
            'codProvincias' => 'Cod Provincias',
        ];
    }

    /**
     * Gets query for [[CodCamioneros0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCamioneros0()
    {
        return $this->hasOne(Camioneros::className(), ['codCamioneros' => 'codCamioneros']);
    }

    /**
     * Gets query for [[CodProvincias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProvincias0()
    {
        return $this->hasOne(Provincias::className(), ['codProvincias' => 'codProvincias']);
    }
}
